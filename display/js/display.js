var month = new Array()
month[0] = "Jan"
month[1] = "Feb"
month[2] = "Mar"
month[3] = "Apr"
month[4] = "May"
month[5] = "Jun"
month[6] = "Jul"
month[7] = "Aug"
month[8] = "Sep"
month[9] = "Oct"
month[10] = "Nov"
month[11] = "Dec"

function getHour(date) {
	hours = date.getHours()
    hours = (hours > 12) ? hours - 12 : hours
    return (hours == '00') ? 12 : hours
}

function getSuffix(date) {
	return (date.getHours() >= 12) ? 'PM' : 'AM'
}

function getMinutes(date) {
	minutes = date.getMinutes()
	return (minutes > 9) ? minutes : '0' + minutes
}

function getTimeStamp(date) {
	// return 1:30 PM - 24 Apr 2017
	return getHour(date) + ':' +
		getMinutes(date) + ' ' +
		getSuffix(date) + ' - ' +
		date.getDate(date) + ' ' +
		month[date.getMonth()] + ' ' +
		date.getFullYear()
}

function openWebSocket() {
	var webSocket = new WebSocket("ws://127.0.0.1:4690/")
	webSocket.onmessage = function(event) {
		var displayData = JSON.parse(event.data)

        var background = "url('" + displayData.photo + "')"
        var date = new Date(displayData.timeStamp)
        console.log("date: " + date)
		document.getElementById("avatar-photo").src = displayData.userAvatar
		document.getElementById("username").innerHTML = displayData.username
		document.getElementById("screen-name").innerHTML = displayData.screenName
		document.getElementById("text").innerHTML = displayData.text
		document.getElementById("photo").style.backgroundImage = background
		document.getElementById("post-date").innerHTML = getTimeStamp(date)
	};
}

setTimeout(openWebSocket, 2000)