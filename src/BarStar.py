#!/usr/local/bin/python3.6

from twitter import *
import asyncio
import os
import websockets
import json
import platform

STOCK_PHOTO = 'http://www.allure-yoga.com/wp-content/uploads/2017/03/a6403d94fba1069ac3f7b3efda568b4d.jpg'


def getTwitterStream():
    return TwitterStream(auth=getOauth(), domain='userstream.twitter.com')


def getLatestTweet():
    t = Twitter(auth=getOauth())
    tweet = t.statuses.user_timeline(count="1", screen_name=getParam('.client_handle'))[0]
    data = {}
    data['userAvatar'] = getUserAvatar(tweet)
    data['username'] = getUserName(tweet)
    data['screenName'] = getScreenName(tweet)
    data['text'] = getText(tweet)
    data['photo'] = getMediaUrl(tweet)
    data['timeStamp'] = tweet['created_at']
    return data


def getOauth():
    consumer_key, consumer_secret = read_token_file(os.path.expanduser('.consumer_keys'))
    access_token, access_token_secret = read_token_file(os.path.expanduser('.access_tokens'))
    return OAuth(consumer_key=consumer_key, consumer_secret=consumer_secret,
                 token=access_token, token_secret=access_token_secret)


def getParam(fileName):
    f = open(fileName)
    return f.readline().strip()


def isClientMentioned(tweet):
    if 'user' in tweet and tweet['user']['screen_name'] == getParam('.client_handle'):
        return True

    userMentions = {}
    if 'entities' in tweet:
        userMentions = tweet['entities']['user_mentions']
    if 'extended_tweet' in tweet:
        userMentions = tweet['extended_tweet']['entities']['user_mentions']
    for userMention in userMentions:
        if userMention['screen_name'] == getParam('.client_handle'):
            return True


def getUserName(tweet):
    if 'user' in tweet:
        return tweet['user']['name']


def getScreenName(tweet):
    if 'user' in tweet:
        return '@' + tweet['user']['screen_name']


def getUserAvatar(tweet):
    if 'user' in tweet:
        return tweet['user']['profile_image_url'].replace('normal.', '200x200.')


def getText(tweet):
    if 'text' in tweet:
        return tweet['text'].split('https')[0]


def getMediaUrl(tweet):
    if 'extended_tweet' in tweet:
        return tweet['extended_tweet']['entities']['media'][0]['media_url']
    if 'entities' in tweet and 'media' in tweet['entities']:
        return tweet['entities']['media'][0]['media_url']
    else:
        return STOCK_PHOTO


def getTimeStamp(tweet):
    return int(tweet['timestamp_ms'])


async def sendToBrowser(host, port):
    data = getLatestTweet()
    # Send latest user tweet to browser
    await host.send(json.dumps(data))
    print("Sending latest tweet...")
    stream = getTwitterStream()
    print('Getting Twitter stream...')
    for tweet in stream.user():
        # Only update page if there is a mention
        if isClientMentioned(tweet):
            print('Tweet received: ' + tweet['created_at'])
            data['userAvatar'] = getUserAvatar(tweet)
            data['username'] = getUserName(tweet)
            data['screenName'] = getScreenName(tweet)
            data['text'] = getText(tweet)
            data['photo'] = getMediaUrl(tweet)
            data['timeStamp'] = getTimeStamp(tweet)
            await host.send(json.dumps(data))


def main():
    # The browser websocket needs to be opened *after* the server starts which is handled by Javascript
    if (platform.system() == 'Linux'):
        os.system('DISPLAY=:0 chromium-browser --kiosk --incognito /home/pi/BarStar/display/' + getParam('.client') + '.html &')
    server = websockets.serve(sendToBrowser, '127.0.0.1', 4690)
    asyncio.get_event_loop().set_debug(True)
    print('Opening python websocket...')
    while(not asyncio.get_event_loop().is_running()):
        print('Starting server event loop...')
        asyncio.get_event_loop().run_until_complete(server)
        asyncio.get_event_loop().run_forever()
        print('Event loop ended.')


main()
