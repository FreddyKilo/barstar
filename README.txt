
##================================##
    API Keys
##================================##
Both files, {.access_tokens} and {.consumer_keys} need to be placed
in the base project directory. These are specific to each client
and do not get pushed to the repo.

##================================##
    UI
##================================##
The client UI needs to be specified in the params file. This should
include file names for the html and css. Any default UI data sources
that are used in the python code should be defined here as well. The
params file will also be gitignore'd