#!/bin/bash

#kill processes if they're running
sudo killall -9 chromium-browse
sudo killall -9 BarStar.py

cd /home/pi/BarStar

#pull latest version then start application
git pull origin master
./src/BarStar.py &
